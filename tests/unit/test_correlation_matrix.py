# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""Tests for Correlation Matrix generation/processing functions."""
import numpy as np
import pytest

from ska_low_cbf_fw_testing.correlation_matrix import (
    cell_from_coords,
    coords_from_vis_index,
    create_metadata_pattern,
    create_raw_visibility_pattern,
    create_visibility_pattern,
    pad_pattern,
    remove_unused_cells,
    triangular_number,
)


def test_pattern_mods_16_unchanged():
    """Ensure pattern padding/rearrangement leaves 16x16 case as-is."""
    raw = create_raw_visibility_pattern(16)
    assert np.all(raw == pad_pattern(raw))
    assert np.all(raw == remove_unused_cells(raw))


def test_pattern_gen_18():
    """Verify 18x18 pattern generation."""
    pattern = create_visibility_pattern(18)
    # the overall matrix is 4 cells, but one is unused
    # 1. [ 1 - 16 x  1 - 16]   x. [      unused     ]
    # 2. [ 1 - 16 x 17 - 32]   3. [17 - 32 x 17 - 32]
    assert pattern.size == 8192 * 3
    first16 = pattern.reshape(768, 32)[0:256, :].reshape(16, 16, 32)
    assert np.all(first16 == create_visibility_pattern(16))
    second_cell = pattern.reshape(768, 32)[256:512, :].reshape(16, 16, 32)
    third_cell = pattern.reshape(768, 32)[512:768, :].reshape(16, 16, 32)

    # 17th row of correlation matrix,
    # so we expect it to start with 16th triangular number plus 1
    tri_16 = triangular_number(16)

    assert second_cell[0, 0, 0] == tri_16 + 1  # 17 x 1
    assert second_cell[0, 1, 0] == tri_16 + 2  # 17 x 2
    assert third_cell[0, 0, 0] == tri_16 + 16 + 1  # 17 x 17
    assert third_cell[0, 1, 0] == 0  # 17 x 18 - not valid

    # 18th row of correlation matrix
    tri_17 = triangular_number(17)
    assert second_cell[1, 0, 0] == tri_17 + 1  # 18 x 1
    assert second_cell[1, 1, 0] == tri_17 + 2  # 18 x 2
    assert third_cell[1, 0, 0] == tri_17 + 16 + 1  # 18 x 17
    assert third_cell[1, 1, 0] == tri_17 + 16 + 2  # 18 x 18

    # count visibility data bytes used
    assert np.count_nonzero(pattern) == 32 * triangular_number(18)


def test_pattern_48():
    """Verify 48x48 pattern generation (6 cells correctly packed)."""
    pattern = create_visibility_pattern(48)
    assert pattern.size == 8192 * 6
    pattern = pattern.reshape(256 * 6, 32)
    second_cell = pattern[256 : 256 * 2, :].reshape(16, 16, 32)
    third_cell = pattern[256 * 2 : 256 * 3, :].reshape(16, 16, 32)
    fourth_cell = pattern[256 * 3 : 256 * 4, :].reshape(16, 16, 32)
    fifth_cell = pattern[256 * 4 : 256 * 5, :].reshape(16, 16, 32)
    sixth_cell = pattern[256 * 5 : 256 * 6, :].reshape(16, 16, 32)
    # assume all bytes of the visibility are the same, just look at byte 0
    assert second_cell[15, 15, 0] == 512 & 0xFF
    assert third_cell[15, 15, 0] == 528 & 0xFF
    assert fourth_cell[0, 0, 0] == 529 & 0xFF
    assert fourth_cell[15, 0, 0] == 1129 & 0xFF
    assert fifth_cell[0, 0, 0] == 545 & 0xFF
    assert fifth_cell[15, 15, 0] == 1160 & 0xFF
    assert sixth_cell[0, 0, 0] == 561 & 0xFF
    assert sixth_cell[15, 15, 0] == 1176 & 0xFF


def test_metadata_size():
    """Check metadata pattern is of expected size."""
    assert create_metadata_pattern(18).nbytes == 3 * 512


@pytest.mark.parametrize(
    "index, row, col",
    [
        (1, 0, 0),
        (2, 1, 0),
        (3, 1, 1),
        (triangular_number(16), 15, 15),
        (triangular_number(16) + 1, 16, 0),
        (triangular_number(16) + 1 + 16, 16, 16),
        (triangular_number(32) + 1, 32, 0),
        (triangular_number(48), 47, 47),
        (triangular_number(49), 48, 48),
    ],
)
def test_coords_from_n(index, row, col):
    """Check visibility index to matrix coordinates conversion."""
    assert coords_from_vis_index(index) == (row, col)


@pytest.mark.parametrize(
    "row, col, cell",
    [
        (0, 0, 1),
        (1, 0, 1),
        (1, 1, 1),
        (15, 15, 1),
        (16, 0, 2),
        (16, 16, 3),
        (47, 0, 4),
        (47, 47, 6),
        (48, 48, 10),
    ],
)
def test_cell_from_coords(row, col, cell):
    """Check cell index calculated correctly from matrix coordinates."""
    assert cell_from_coords(row=row, col=col) == cell
