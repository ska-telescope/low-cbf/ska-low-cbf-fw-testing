# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""Pytest Configuration."""
import os
import platform
from typing import Iterator

import pytest
from ska_low_cbf_fpga import FpgaPersonality, create_driver_map_info, mem_parse
from ska_low_cbf_proc.icl.correlator.corr_fpga import CorrFpga
from ska_low_cbf_sw_cnic import CnicFpga

host_fpga_bdfs = {
    "fpga-dev07": {"u55c": ("0000:08:00.1",), "u280": ("0000:07:00.1",)},
}
"""PCIe BDF addresses of FPGA cards. Keyed by host name, then FPGA type."""


def bdf_generator(fpga_type: str) -> Iterator[str]:
    """Yield BDFs according to our host & FPGA type."""
    # Use HOSTNAME var if set, but GitLab runner doesn't have HOSTNAME set by default
    hostname = os.getenv("HOSTNAME", default=platform.node())
    print(f"Host name: {hostname}, FPGA type: {fpga_type}")
    yield from host_fpga_bdfs[hostname][fpga_type]


unused_u55c = bdf_generator("u55c")
unused_u280 = bdf_generator("u280")


@pytest.fixture(scope="module")
def cnic() -> CnicFpga:
    """Create a CnicFpga object."""
    memory = mem_parse("2Gs:2Gs:2Gs:2Gs")

    bdf = next(unused_u280)
    print(f"Creating u280 CnicFpga with BDF {bdf}")
    driver, args_map, hw_info = create_driver_map_info(
        device=bdf, firmware_path="./cnic.xclbin", mem_config=memory
    )
    # fpga-dev07 has PTP connected to port B of both FPGA cards
    fpga = CnicFpga(driver, args_map, hw_info, ptp_source_b=True)
    print_fpga_firmware_details(fpga)
    return fpga


@pytest.fixture(scope="module")
def correlator() -> CorrFpga:
    """Create a Correlator FPGA object."""
    memory = mem_parse("3Gi:3Gi:3Gi:512Ms:512Mi")

    bdf = next(unused_u55c)
    print(f"Creating u55c CorrFpga with BDF {bdf}")
    driver, args_map, hw_info = create_driver_map_info(
        device=bdf, firmware_path="./correlator.xclbin", mem_config=memory
    )
    fpga = CorrFpga(driver, args_map, hw_info)
    print_fpga_firmware_details(fpga)
    return fpga


def print_fpga_firmware_details(fpga: FpgaPersonality):
    """Print FPGA firmware details."""
    for attr in ("fw_personality", "fw_version"):
        print(getattr(fpga, attr).description, getattr(fpga, attr).value)
