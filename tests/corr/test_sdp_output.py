# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""
SDP Packetiser Output tests.

We use a special visibility data array:
16x16 grid(s)
each value 32 bytes
8kB in total for one cell

A 17x17 config will use 24k bytes  (lots of empty bits...)

1. [ 1-16 x  1-16]  x. [    unused   ]
2. [ 1-16 x 17-32]  3. [17-32 x 17-32]

e.g. first value to be 0x0101_0101_0101_0101... (32Bytes!)
then 0x0202_0202..., on to 0x0F0F_0F0F... and so on
"""
import math
import warnings
from time import sleep

import numpy as np
import pytest
from dpkt.pcap import UniversalReader
from pytest_check import check

from ska_low_cbf_fw_testing.correlation_matrix import (
    cell_from_coords,
    coords_from_vis_index,
    create_metadata_pattern,
    create_visibility_pattern,
    meta_datatype,
    print_vis_hex,
    triangular_number,
    vis_datatype,
)

MAX_MATRIX_DATA_PER_PACKET = 8192
"""Maximum amount of Correlation Matrix (Visibility) data in one packet [Bytes]"""

SPEAD_HEADER_LEN = 64  # Low CBF SDP SPEAD Protocol v3, with VisFl
ETH_IP_UDP_HEADERS_LEN = 42
TOTAL_HEADERS_LEN = ETH_IP_UDP_HEADERS_LEN + SPEAD_HEADER_LEN
"""Length of all packet headers (Ethernet, IP, UDP, SPEAD)"""
TIMESTAMP_LEN = 8  # epoch offset
HBM_INDEX = 4


# TODO start at 256M-4k and wrap around back to zero
@pytest.mark.parametrize("hbm_offset", (0, 128 << 20))
@pytest.mark.parametrize("pattern_size", list(range(239, 257)))
def test_sdp_packets(request, cnic, correlator, pattern_size, hbm_offset):
    """Check SDP Output Packets."""
    # Load pattern into HBM
    pattern = create_visibility_pattern(pattern_size)
    with open(f"build/{request.node.name}_visibility.txt", "w") as data_dump:
        print_vis_hex(pattern, file=data_dump)
    metadata = create_metadata_pattern(pattern_size)
    with open(f"build/{request.node.name}_metadata.txt", "w") as metadata_dump:
        print_vis_hex(metadata, file=metadata_dump)

    # 34 = vis_datatype + meta_datatype bytes
    total_matrix_bytes = (pattern_size * (pattern_size + 1) * 34) // 2
    """Total size of the visibilities & metadata (not entire SPEAD payload)"""
    heap_size = TIMESTAMP_LEN + total_matrix_bytes
    """Total heap size (i.e. complete SPEAD payload for all packets)"""
    matrix_bytes_per_packet = min(MAX_MATRIX_DATA_PER_PACKET, heap_size)
    """Visibility & meta data bytes in each packet"""
    n_packets_per_heap = math.ceil(heap_size / MAX_MATRIX_DATA_PER_PACKET)
    n_packets = 10 * n_packets_per_heap
    print(f"Heap size {heap_size} B, {n_packets_per_heap} packets per heap")

    # erase any previous HBM contents
    correlator.write_memory(HBM_INDEX, np.zeros(512 << 20, dtype=np.uint8), 0)
    # Lower 256M for visibilities
    # Upper 256M for metadata
    # metadata is 512 bytes for each 16x16 cell (2 Bytes per value)
    correlator.write_memory(HBM_INDEX, pattern, offset_bytes=0 + hbm_offset)
    correlator.write_memory(
        HBM_INDEX, metadata, offset_bytes=(256 << 20) + (hbm_offset // 16)
    )

    correlator.hbm_rd_debug.testmode_select = True
    correlator.hbm_rd_debug.testmode_hbm_start_addr = hbm_offset
    correlator.hbm_rd_debug.testmode_subarray = 0
    correlator.hbm_rd_debug.testmode_freqindex = 0
    correlator.hbm_rd_debug.testmode_time_ref = 0
    correlator.hbm_rd_debug.testmode_time_ref_upper_bytes = 0
    correlator.hbm_rd_debug.testmode_row = 0
    correlator.hbm_rd_debug.testmode_row_count = pattern_size

    with open(f"build/{request.node.name}.pcap", "w+b") as pcap_file:
        print("PCAP:", pcap_file.name)
        cnic.reset()
        cnic.receive_pcap(
            out_filename=pcap_file.name,
            packet_size=TOTAL_HEADERS_LEN,  # all packets should be bigger than this
            n_packets=n_packets,
        )

        # 6144 + subarray number
        correlator.spead_sdp.data[6144] = heap_size
        correlator.spead_sdp.enable()
        _testmode_send_packets(correlator, n_packets)
        wait_receive(cnic, correlator)

        pcap_file.seek(0)
        reader = UniversalReader(pcap_file)
        max_n = triangular_number(pattern_size)

        matrix = b""
        for n_packet, (_, packet) in enumerate(reader):
            n = 1  # visibility data matrix item counter
            # Collect packets until we have enough data, discarding headers and spurious
            # data at end of packet
            matrix_start = TOTAL_HEADERS_LEN
            if not matrix:
                # first packet contains a time value
                matrix_start += TIMESTAMP_LEN
            # with multiple packets, last packet is likely shorter than the rest
            matrix_end = matrix_start + min(
                matrix_bytes_per_packet,
                total_matrix_bytes - len(matrix),
            )

            matrix += packet[matrix_start:matrix_end]
            if len(packet) > matrix_end:
                warnings.warn(
                    f"Packet {n_packet} longer ({len(packet)}) than "
                    f"expected ({matrix_end})"
                )

            if len(matrix) < total_matrix_bytes:
                # gather more payload data from next packet
                continue

            print(f"collected {len(matrix)} bytes")
            for offset in range(0, len(matrix), 34):
                chunk = matrix[offset : offset + 34]
                vis = np.frombuffer(chunk[:32], dtype=vis_datatype)[0]
                meta = int(np.frombuffer(chunk[32:], dtype=meta_datatype)[0])
                if n <= max_n:
                    coordinates = coords_from_vis_index(n)
                    cell = cell_from_coords(*coordinates)
                    expected_vis = n & 0xFF
                    # convert vis to set to show unique values for ease of readability
                    with check:  # check lets us see all the errors
                        assert np.all(vis == expected_vis), (
                            f"Data mismatch. Visibility #{n}"
                            f" Cell {cell} row,col: {coordinates}."
                            f" Expected {expected_vis}, got {set(vis)}."
                        )
                    expected_meta = cell | (expected_vis << 8)
                    with check:
                        assert meta == expected_meta, (
                            "Metadata mismatch."
                            f" Visibility #{n} row,col: {coordinates}."
                            f" Expected cell {cell}, got cell {meta >> 8}."
                            f" Expected count {n & 0xFF}, got count {meta & 0xFF}."
                        )
                else:
                    assert np.all(vis == 0)
                    assert meta == 0

                n += 1
            if check.any_failures():  # don't need to see the same errors 10 times over
                print("Stopping due to failures")
                break
            matrix = b""  # empty payload - signal to collect a fresh one
    print(f"{n - 1} visibility items checked")


def wait_receive(cnic, correlator):
    """Wait for CNIC to receive all packets."""
    n_sleeps = 0
    while not cnic.finished_receive:
        print()
        print("CNIC Rx packets:", cnic.hbm_pktcontroller.rx_packet_count.value)
        print("Corr Tx packets:", correlator.system.eth100g_tx_total_packets.value)
        n_sleeps += 1
        if n_sleeps >= 6:
            cnic.stop_receive()
            cnic.reset()
            raise RuntimeError("Did not receive packets!")
        sleep(5)


def _testmode_send_packets(correlator, n_packets):
    """Tell Correlator HBM read debug peripheral to send packets."""
    for _ in range(n_packets):
        correlator.hbm_rd_debug.testmode_load_instruct = True
        correlator.hbm_rd_debug.testmode_load_instruct = False
