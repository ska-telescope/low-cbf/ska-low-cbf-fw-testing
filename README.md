# ska-low-cbf-fw-testing
Firmware tests for Low CBF

## Purpose
This repository/project exists purely to provide a test pipeline that can be triggered
from other firmware build CI pipelines.

Firmware builds take a long time! By splitting these tests into a separate pipeline,
we can more easily modify (and test) the tests.

## Project Avatar (Repository Icon)
[Flask icons created by Smashicons - Flaticon](https://www.flaticon.com/free-icons/flask "flask icons")

# Usage

To run locally, set the FPGA_TYPE variable, and run `make python-test`:

```shell
FPGA_TYPE=u55c make python-test
```
Optionally, you can add [pytest](https://pytest.org) flags with the
`PYTHON_VARS_AFTER_PYTEST` variable.

Note we use the [pytest-check plugin](https://pypi.org/project/pytest-check/)
to print multiple errors to aid firmware debugging. You can limit the number of such
messages using `--check-max-fail` or `--check-max-report`.

e.g. Here we tell pytest to be very verbose, capture stdout, select tests containing
'50', and stop after two failed checks:

```shell
FPGA_TYPE=u55c PYTHON_VARS_AFTER_PYTEST="-vvs -k 50 --check-max-fail=2" make python-test
```

# Developer's Guide

* The Makefiles here are inherited from
  [ska-cicd-makefile](https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile).
  * Refer to docs at that repo, or use `make help` for details.
  * This link is via a git submodule, so use the `--recursive` flag when cloning
this repository, or run `git submodule update --init --recursive` afterwards.
* A pre-commit config is provided to try and format your code before it gets
rejected by the CI pipeline lint checkers.
  * Install [pre-commit](https://pre-commit.com/) - `pip3 install pre-commit`
  * Activate the pre-commit checks for the repo - `pre-commit install`
  * From now on your commits will be formatted automatically. Note that you
have to add & commit again any files changed by pre-commit.
* A git hook is provided that may help comply with SKA commit message rules.
You can install the hook with `cp -s "$(pwd)/resources/git-hooks"/* .git/hooks`.
Once installed, the hook will insert commit messages to match the JIRA ticket
from the branch name.
e.g. On branch `perentie-1350-new-base-classes`:
```console
ska-low-cbf$ git commit -m "Add git hook note to README"
Branch perentie-1350-new-base-classes
Inserting PERENTIE-1350 prefix
[perentie-1350-new-base-classes 3886657] PERENTIE-1350 Add git hook note to README
 1 file changed, 7 insertions(+)
```
You can see the modified message above, and confirming via the git log:
```console
ska-low-cbf$ git log -n 1 --oneline
3886657 (HEAD -> perentie-1350-new-base-classes) PERENTIE-1350 Add git hook note to README
```

# GitLab Runner Notes

* We need `poetry` installed on the runner.
Check official instructions, but at time of writing this worked:

```shell
curl -sSL https://install.python-poetry.org | POETRY_HOME=/opt/poetry POETRY_VERSION=1.3.2 python3 -
ln -s /opt/poetry/bin/poetry /usr/bin/poetry
```
