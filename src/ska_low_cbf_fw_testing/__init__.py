"""Top-level package for ska-low-cbf-fw-testing."""

__author__ = """Andrew Bolin"""
__email__ = "andrew.bolin@csiro.au"
__version__ = "0.0.1"
