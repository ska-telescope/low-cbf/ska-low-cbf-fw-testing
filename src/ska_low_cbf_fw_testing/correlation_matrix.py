# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2024 CSIRO
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.

"""Functions & constants to help deal with Correlation data matrix."""
import math

import numpy as np

VIS_DATA_BYTES = 32
vis_datatype = np.dtype(f"{VIS_DATA_BYTES}B")
meta_datatype = np.uint16  # pylint: disable=invalid-name


def triangular_number(n: int) -> int:
    """Calculate nth triangular number."""
    return (n * (n + 1)) // 2


def create_raw_visibility_pattern(size: int) -> np.ndarray:
    """Create a pattern of visibility data that uses a square matrix."""
    pattern = np.zeros((size, size), dtype=vis_datatype)
    n = 0
    for i in range(size):
        for j in range(i + 1):
            n += 1
            pattern[i, j] = n & 0xFF  # value repeats for each byte in the visibility
    return pattern


def cell_from_coords(row, col) -> int:
    """
    Get cell number from data item coordinates in full matrix.

    :param row: 0-based
    :param col: 0-based
    :return: Cell number, 1-based. Unused cells are not counted.
    """
    # TODO - there must be a more efficient way to calculate this...
    size = math.ceil(max(row + 1, col + 1) / 16) * 16
    n_cells = (size * size) // (16 * 16)
    cell_map_size = int(math.sqrt(n_cells))
    cell_map = np.zeros((cell_map_size, cell_map_size), dtype=np.uint8)
    n = 1
    for map_row in range(cell_map_size):
        for map_col in range(map_row + 1):
            cell_map[map_row, map_col] = n
            n += 1
    return int(cell_map[row // 16, col // 16])


def create_raw_metadata_pattern(size: int) -> np.ndarray:
    """Create a pattern of metadata values in a square array of given size."""
    pattern = np.zeros((size, size), dtype=meta_datatype)
    n = 0
    for row in range(size):
        for col in range(row + 1):
            n += 1
            # TODO - inefficient to create the whole map each time...
            cell = cell_from_coords(row, col)
            pattern[row, col] = (cell << 8) | (n & 0xFF)
    return pattern


def pad_pattern(pattern: np.ndarray, dtype=vis_datatype) -> np.ndarray:
    """Pad pattern to a square array where the size is a multiple of 16."""
    assert pattern.shape[0] == pattern.shape[1]
    size = pattern.shape[0]
    padded_size = math.ceil(size / 16) * 16
    padded_pattern = np.zeros((padded_size, padded_size), dtype=dtype)
    padded_pattern[0 : pattern.shape[0], 0 : pattern.shape[1]] = pattern
    return padded_pattern


def remove_unused_cells(pattern: np.ndarray, dtype=vis_datatype) -> np.ndarray:
    """Remove unused 16x16 cells from a square pattern."""
    assert pattern.shape[0] == pattern.shape[1]
    assert pattern.shape[0] % 16 == 0
    n_cells = (pattern.shape[0] * pattern.shape[1]) // (16 * 16)
    sqrt_n_cells = int(math.sqrt(n_cells))
    assert sqrt_n_cells**2 == n_cells
    n_valid_cells = triangular_number(sqrt_n_cells)

    # rather than cut & paste, this *should* be possible with `reshape` and a mask...
    # but the cut & paste works today
    final_pattern = np.zeros((n_valid_cells, 16, 16), dtype=dtype)
    cell = 0
    for i in range(sqrt_n_cells):
        for j in range(i + 1):
            chunk = pattern[i * 16 : (i + 1) * 16, j * 16 : (j + 1) * 16]
            final_pattern[cell, :, :] = chunk
            cell += 1

    if dtype == vis_datatype:
        return final_pattern.reshape((n_valid_cells * 16, 16, 32))
    if dtype == meta_datatype:
        return final_pattern.reshape(n_valid_cells * 16, 16)
    raise NotImplementedError(f"Can't handle dtype {dtype}")


def create_visibility_pattern(size: int) -> np.ndarray:
    """Create visibility data pattern."""
    return remove_unused_cells(pad_pattern(create_raw_visibility_pattern(size)))


def create_metadata_pattern(size: int) -> np.ndarray:
    """Create metadata pattern."""
    return remove_unused_cells(
        pad_pattern(create_raw_metadata_pattern(size), dtype=meta_datatype),
        dtype=meta_datatype,
    )


def print_vis_hex(data: np.ndarray, file=None) -> None:
    """
    Print an array of visibility data in hexadecimal.

    :param data: `n` by `m` by 32 Byte array of visibilities (probably `n` and `m` are
      multiples of 16, but they don't have to be here)
    """
    n_lines = data.shape[0] * data.shape[1]
    n_digits = math.ceil(math.log10(n_lines))  # digits required for line number
    n = 0
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            print(f"{n:>{n_digits}d}", end=" ", file=file)
            print("0x" + "".join(f"{i:02x}" for i in data[i, j].tobytes()), file=file)
            n += 1


def coords_from_vis_index(n: int) -> (int, int):
    """
    Calculate coordinates in matrix from a visibility sequence number.

    e.g.

    1
    2 3
    4 5 6

    n=1 => (0,0)
    n=5 => (2,1)

    :param n: 1-based visibility counter
    :returns: (row, column)
    """
    n = int(n)
    if n <= 0:
        raise ValueError("n must be 1+")

    # find row (down from top)
    row = 0
    while triangular_number(row + 1) < n:
        row += 1
    # find column (rightwards from left)
    start_of_row = triangular_number(row) + 1
    col = n - start_of_row
    return row, col
